package com.coreservlets.intentfilter1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

/** Demonstrates the simplest form of Activity switching: 
 *  invoking another Activity by class name. There are
 *  two variations here: invoking the other Activity
 *  with no data, and invoking the other Activity and
 *  sending data in the "extras" data. Also see IntentFilter2Activity,
 *  which demonstrates how to register an intent filter for a URI
 *  so that the Activity can be invoked in a more general-purpose manner,
 *  without having to know the class name.
 *  <p>
 *  From <a href="http://www.coreservlets.com/android-tutorial/">
 *  the coreservlets.com Android programming tutorial series</a>.
 */

public class IntentFilter1Activity extends Activity {
    Switch s;
    Button help;
    /** Initializes the app when it is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        s = findViewById(R.id.detalhes);
        help = findViewById(R.id.help);
        help.setOnClickListener(new View.OnClickListener() {    //Crio uma intenção de busca pra um banco qualquer,
                                                                // qualquer browser ou aplicativo que suporte buscas
                                                                //pode capturar e tratar disso. Exercício 3
            @Override
            public void onClick(View view) {
                Uri webpage = Uri.parse("http://www.caixa.gov.br/voce/credito-financiamento/credito-pessoal/Paginas/default.aspx");
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
    }
    
    /** Switches to the LoanCalculatorActivity when the associated button is clicked. 
     *  Does not send any extra data, so the loan calculator will use its defaults.
     *  You must also register the new Activity in AndroidManifest.xml. 
     */
    public void showLoanPayments1(View clickedButton) {
        Intent activityIntent = 
                new Intent(this, LoanCalculatorActivity.class);
        Bundle args = new Bundle();
        args.putBoolean("SWITCH_STATE",s.isChecked());      //adiciono um boolean que diz pras activities subsequentes se
                                                            //o usuário quer ou nao ver os detalhes do pagamento
        activityIntent.putExtras(args);
        startActivity(activityIntent);
    }
    
    /** Switches to the LoanCalculatorActivity when the associated button is clicked. 
     *  Sends extra data, so the loan calculator will use the values sent instead of its defaults. 
     */
    public void showLoanPayments2(View clickedButton) {
        Intent activityIntent = 
                new Intent(this, LoanCalculatorActivity.class);
        Bundle args = LoanBundler.makeRandomizedLoanInfoBundle();
        args.putBoolean("SWITCH_STATE",s.isChecked());      //adiciono um boolean que diz pras activities subsequentes se
                                                            //o usuário quer ou nao ver os detalhes do pagamento
        activityIntent.putExtras(args);
        startActivity(activityIntent);
    }
    
    /** Switches to the TabbedActivity when the associated button is clicked. */
    
    public void showTabs(View clickedButton) {
        Intent activityIntent = new Intent(this, TabbedActivity.class);
        Bundle args = new Bundle();
        args.putBoolean("SWITCH_STATE",s.isChecked());      //adiciono um boolean que diz pras activities subsequentes se
                                                            //o usuário quer ou nao ver os detalhes do pagamento
        activityIntent.putExtras(args);
        startActivity(activityIntent);
    }
}